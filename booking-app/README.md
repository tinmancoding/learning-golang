# Golang Tutorial for Beginners | Full Go course

This project is created from a Youtube video originally posted by *TEchWorld with Nana*. 
Check out the video here: https://www.youtube.com/watch?v=yyUHQIec83I

## Changes

I've added some minor changes at the end of the tutorial, to better demonstrate how goroutines can be waited using a Waitgroup.

